# Transitions in Spring 
use of @Transactional to avoid data inconsistency

Sender Army sends amount of soldiers to Receiver Army. If the process fails but we update Receiver Army numbers, the data will be inconsistent

Project:

DTO ,@Transactional

JDBC,datasource

H2

* A transaction is a set of operations that change data, which either execute together or not at all. In a real-world scenario, almost any use case should be the subject of a transaction to avoid data inconsistencies.

* If any of the operations fail, the app restores the data to how it was at the beginning of the transaction. When that happens, we say that the transaction rolls back.

* If all the operations succeed, we say the transaction commits, which means the app persists all the changes the use case execution did.

* To implement transactional code in Spring, you use the @Transactional annotation. You use the @Transactional annotation to mark a method you expect Spring to wrap in a transaction. You can also annotate a class with @Transactional to tell Spring that any class methods need to be transactional.

* At execution, a Spring aspect intercepts the methods annotated with @Transactional. The aspect starts the transaction, and if an exception occurs the aspect rolls back the transaction. If the method doesn’t throw an exception, the transaction commits, and the app persists the method’s changes.
