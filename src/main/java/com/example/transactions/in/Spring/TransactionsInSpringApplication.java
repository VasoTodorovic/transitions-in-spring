package com.example.transactions.in.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionsInSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionsInSpringApplication.class, args);
	}

}
