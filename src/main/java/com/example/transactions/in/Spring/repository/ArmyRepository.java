package com.example.transactions.in.Spring.repository;


import com.example.transactions.in.Spring.model.Army;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class ArmyRepository {

  private final JdbcTemplate jdbc;

  public ArmyRepository(JdbcTemplate jdbc) {
    this.jdbc = jdbc;
  }

  public Army findAccountById(long id) {
    String sql = "SELECT * FROM  armies WHERE id = ?";
    return jdbc.queryForObject(sql, new ArmyRowMapper(), id);
  }

  public List<Army> findAllArmies() {
    String sql = "SELECT * FROM armies";
    return jdbc.query(sql, new ArmyRowMapper());
  }

  public void changeAmount(long id, Integer soldiers) {
    String sql = "UPDATE armies SET soldiers = ? WHERE id = ?";
    jdbc.update(sql, soldiers, id);
  }
}
