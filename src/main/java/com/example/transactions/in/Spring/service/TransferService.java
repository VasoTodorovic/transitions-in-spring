package com.example.transactions.in.Spring.service;


import com.example.transactions.in.Spring.repository.ArmyRepository;
import com.example.transactions.in.Spring.model.Army;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TransferService {

  private final ArmyRepository accountRepository;


  public TransferService(ArmyRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Transactional
  public void transferSoldires(long idSender, long idReceiver, Integer amount) {
    Army sender = accountRepository.findAccountById(idSender);
    Army receiver = accountRepository.findAccountById(idReceiver);

    Integer senderNewAmount = sender.getSoldiers() - amount;
    Integer receiverNewAmount = receiver.getSoldiers()+amount;


    accountRepository.changeAmount(idSender, senderNewAmount);
    accountRepository.changeAmount(idReceiver, receiverNewAmount);
    if(senderNewAmount<0 || receiverNewAmount<0){
      throw new RuntimeException("we dont have that amount of soldiers,Sir");
    }
  }

  public List<Army> getAllArmies() {
    return accountRepository.findAllArmies();
  }
}
