package com.example.transactions.in.Spring.dto;

import java.math.BigDecimal;

public class TransferRequest {

  private long senderArmyId;
  private long receiverArmyId;
  private Integer soldiers;

  public long getSenderArmyId() {
    return senderArmyId;
  }

  public void setSenderArmyId(long senderArmyId) {
    this.senderArmyId = senderArmyId;
  }

  public long getReceiverArmyId() {
    return receiverArmyId;
  }

  public void setReceiverArmyId(long receiverArmyId) {
    this.receiverArmyId = receiverArmyId;
  }

  public Integer getSoldiers() {
    return soldiers;
  }

  public void setAmount(Integer amount) {
    this.soldiers = amount;
  }
}
