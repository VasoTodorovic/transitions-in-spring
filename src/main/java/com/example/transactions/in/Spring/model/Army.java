package com.example.transactions.in.Spring.model;

import java.math.BigDecimal;

public class Army {

    private long id;
    private String name;
    private Integer soldiers;
    private String comander;
    public Army() {

    }

    public Army(long id, String name, Integer soldiers, String comander) {
        this.id = id;
        this.name = name;
        this.soldiers = soldiers;
        this.comander = comander;
    }

    public String getComander() {
        return comander;
    }

    public void setComander(String comander) {
        this.comander = comander;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSoldiers() {
        return soldiers;
    }

    public void setSoldiers(Integer soldiers) {
        this.soldiers = soldiers;
    }
}
