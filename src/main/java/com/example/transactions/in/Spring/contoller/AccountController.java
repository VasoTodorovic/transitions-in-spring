package com.example.transactions.in.Spring.contoller;

import java.util.List;

import com.example.transactions.in.Spring.dto.TransferRequest;
import com.example.transactions.in.Spring.service.TransferService;
import com.example.transactions.in.Spring.model.Army;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

  private final TransferService transferService;

  public AccountController(TransferService transferService) {
    this.transferService = transferService;
  }

  @PostMapping("/transfer")
  public void transferSoldiers(
      @RequestBody TransferRequest request
      ) {
    transferService.transferSoldires(
        request.getSenderArmyId(),
        request.getReceiverArmyId(),
        request.getSoldiers());
  }

  @GetMapping("/armies")
  public List<Army> getAllAccounts() {
    return transferService.getAllArmies();
  }
}
