package com.example.transactions.in.Spring.repository;


import com.example.transactions.in.Spring.model.Army;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArmyRowMapper implements RowMapper<Army> {

  @Override
  public Army mapRow(ResultSet resultSet, int i) throws SQLException {
    Army a = new Army();
    a.setId(resultSet.getInt("id"));
    a.setName(resultSet.getString("name"));
    a.setSoldiers(resultSet.getInt("soldiers"));
    a.setComander(resultSet.getString("commander"));
    return a;
  }
}
