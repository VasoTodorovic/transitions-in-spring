package com.example.transactions.in.Spring;

import com.example.transactions.in.Spring.model.Army;
import com.example.transactions.in.Spring.repository.ArmyRepository;
import com.example.transactions.in.Spring.service.TransferService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Incubating;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@AutoConfigureMockMvc
@SpringBootTest
class TransactionsInSpringApplicationTests {
    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Endpoint will not throw Exception")
    public void testPost() throws Exception {
        mvc.perform(post("/transfer").contentType(MediaType.APPLICATION_JSON)

                        .content("{\"senderArmyId\":1,\n" +
                                "\"receiverArmyId\":2,\n" +
                                "\"soldiers\":500\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
    @Test
    @DisplayName("Endpoint will throw Exception")
    public void testPostTransactionError() throws Exception {
        mvc.perform(post("/transfer").contentType(MediaType.APPLICATION_JSON)

                        .content("{\"senderArmyId\":1,\n" +
                                "\"receiverArmyId\":2,\n" +
                                "\"soldiers\":1500\n" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());

    }
    @Test
    @DisplayName("Endpoint will return data initiated ")
    public void testGet() throws Exception {
        mvc.perform(get("/armies")).andReturn().getResponse().getContentAsString()
        .contains("Second Serbian Army")
        ;

    }
}
